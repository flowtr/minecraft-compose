# minecraft-compose

A easy-to-use minecraft docker compose setup that also works with podman.

## Usage

### Copying the Config Files

```bash
cp ./proxy.example.yml ./proxy.yml
cp ./.env.example ./.env
```

You will want to edit the **.env** file in order make changes to the data path, the server motd, and the admin user for the proxy.
You can add additional servers in **proxy.yml** at the bottom of the file.

### Running the Server Setup

```bash
docker-compose up -d
```

You should then be able to accessible the minecraft server on port 25565 of your machine.

